const Router = require('koa-better-router');
const koaBody = require('koa-body');

const controller = require('./controllers/user');

module.exports = () => {
  let router = Router({ prefix: '/api' }).loadMethods();
  router.get('/user', controller.getUser);
  router.get('/user/:id', controller.getOne);
  router.post('/user', koaBody(), controller.createUser);
  router.put('/user/:id', koaBody(), controller.updateUser);
  router.delete('/user/:id', controller.deleteUser);
  return router.middleware();
};
